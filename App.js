import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import LoginScreen from './Src/Screens/LoginScreen'
import DashBoardScreen from './Src/Screens/DashBoard'
const StartNavigator = createStackNavigator(
  {
    Login: {screen: LoginScreen},
    DashBoard: {screen: DashBoardScreen},
  },
  {headerMode: 'none', initialRouteName: 'Login'},
);
const App = createAppContainer(StartNavigator);
export default App;
