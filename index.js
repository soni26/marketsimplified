import {AppRegistry, SafeAreaView} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import React, {Component} from 'react';
console.disableYellowBox = true; //to disable warnings
class MainApp extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <App />
      </SafeAreaView>
    );
  }
}

AppRegistry.registerComponent(appName, () => MainApp);
