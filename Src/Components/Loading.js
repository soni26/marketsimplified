import React from 'react';
import {StyleSheet, View, Image} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
export default class Loading extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.viewStyle}>
      <Image source={require('../Utils/logo.png')} style={styles.image} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  image: {
    width: wp('50%'),
    height: hp('15%'),
    resizeMode: 'contain',
    marginVertical: hp('10%'),
  },
});
