import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  BackHandler,Alert
} from 'react-native';
import {Container} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol,
} from 'react-native-responsive-screen';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import Loading from '../Components/Loading';
class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: '',
      userDB: [
        {
          userName: 'user1',
          password: 'user@1',
        },
        {
          userName: 'user2',
          password: 'user@2',
        },
      ],
      loading: true,
      exitApp: 0,
    };
  }
  async componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', () => this.backAction());
    var user = await AsyncStorage.getItem('user');
    if (user != null) {
      this.props.navigation.navigate('DashBoard');
    }
    this.setState({loading: false});
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', () =>
      this.backAction(),
    );
    rol();
  }

  backAction() {
    if (this.state.exitApp === 0) {
      this.setState({exitApp: this.state.exitApp + 1});
      return true;
    } else if (this.state.exitApp === 1) {
      Alert.alert(
        `Exit!!! `,
        `Are you sure, You want to exit?`,
        [
          {
            text: 'Not now',
            onPress: () => this.setState({exitApp: 0}),
            style: 'cancel',
          },
          {
            text: 'Yes Please',
            onPress: () => {
              BackHandler.exitApp();
            },
          },
        ],
        {cancelable: false},
      );
      return true;
    }
  }
  async handleLogin() {
    const {userName, password} = this.state;
    this.setState({userName: '', password: ''});
    if (userName != '' && password != '') {
      var check = this.state.userDB.filter(
        (item) => item.userName == userName && item.password == password,
      );
      console.log(check);
      if (check.length > 0) {
        await AsyncStorage.setItem('user', JSON.stringify(check[0]));
        this.props.navigation.navigate('DashBoard');
      } else {
        Toast.show(`Invalid user name or password`);
      }
    } else {
      Toast.show(`Please fill user name and password`);
    }
  }
  render() {
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: '#FFF',
      },
      content: {
        backgroundColor: '#5e994d2e',
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingVertical: hp('5%'),
      },
      input: {
        width: wp('80%'),
        borderBottomWidth: 1,
        marginBottom: hp('1%'),
        borderBottomColor: '#5e994d',
        color: '#5e994d',
      },
      submitButton: {
        marginVertical: hp('2%'),
        paddingVertical: hp('1.5%'),
        width: wp('80%'),
        backgroundColor: '#5e994d',
        alignItems: 'center',
        borderRadius: wp('1%'),
      },
      submitText: {
        color: '#FFF',
        fontSize: wp('5%'),
        fontWeight: 'bold',
      },
      imageView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },
      image: {
        width: wp('50%'),
        height: hp('15%'),
        resizeMode: 'contain',
        marginVertical: hp('10%'),
      },
    });
    if (this.state.loading) {
      return <Loading />;
    }
    return (
      <Container style={styles.container}>
        <View style={styles.imageView}>
          <Image source={require('../Utils/logo.png')} style={styles.image} />
        </View>
        <View style={styles.content}>
          <TextInput
            style={styles.input}
            underlineColorAndroid="transparent"
            placeholder="User Name"
            value={this.state.userName}
            autoCapitalize="none"
            onChangeText={(userName) => {
              this.setState({userName});
            }}
          />
          <TextInput
            style={styles.input}
            secureTextEntry={true}
            value={this.state.password}
            underlineColorAndroid="transparent"
            placeholder="Password"
            autoCapitalize="none"
            onChangeText={(password) => {
              this.setState({password});
            }}
          />
          <TouchableOpacity
            onPress={() => {
              this.handleLogin();
            }}
            style={styles.submitButton}>
            <Text style={styles.submitText}>Login</Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}
export default LoginScreen;
