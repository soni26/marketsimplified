import React from 'react';
import {Container, Icon, Header, Right, Left} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as lor,
  removeOrientationListener as rol,
} from 'react-native-responsive-screen';
import {
  FlatList,
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  BackHandler,
  Alert,
  Modal,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Loading from '../Components/Loading'
var timeOut = 0;
class DashboardScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      filterList: [],
      data: [],
      id: '',
      loading: true,
      exitApp: 0,
      modal: false,
    };
  }
  async componentDidMount() {
    var data = await AsyncStorage.getItem('data');
    if (data != null) {
      data = JSON.parse(data);
    } else {
      data = await fetch(`http://hplussport.com/api/products/`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log('data', responseJson);
          return responseJson;
        })
        .catch((error) => {
          return '';
        });
      await AsyncStorage.setItem('data', JSON.stringify(data));
    }
    this.setState({
      data,
      filterList: data,
      loading:false
    });
    BackHandler.addEventListener('hardwareBackPress', () => this.backAction());
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', () =>
      this.backAction(),
    );
    rol();
  }
  backAction() {
    if (this.state.exitApp === 0) {
      this.setState({exitApp: this.state.exitApp + 1});
      return true;
    } else if (this.state.exitApp === 1) {
      Alert.alert(
        `Exit!!! `,
        `Are you sure, You want to exit?`,
        [
          {
            text: 'Not now',
            onPress: () => this.setState({exitApp: 0}),
            style: 'cancel',
          },
          {
            text: 'Yes Please',
            onPress: () => {
              BackHandler.exitApp();
            },
          },
        ],
        {cancelable: false},
      );
      return true;
    }
  }
  renderItem(item, index) {
    var styles = StyleSheet.create({
      header: {
        color: '#000',
        fontSize: wp('5%'),
      },
      description: {
        color: '#757576',
        textAlign: 'justify',
      },
      image: {
        width: wp('40%'),
        height: wp('40%'),
      },
      content: {
        backgroundColor: '#FFF',
        flexDirection: 'row',
        margin: wp('1%'),
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#C0C0C0',
      },
      textView: {flex: 1},
    });
    return (
      <TouchableOpacity
        onLongPress={() =>
          this.setState({
            id: item,
            modal: true,
          })
        }
        style={styles.content}>
        <View>
          <Image style={styles.image} source={{uri: item.image}} />
        </View>
        <View style={styles.textView}>
          <Text style={styles.header}>{item.name}</Text>
          <Text style={styles.description}>{item.description}</Text>
        </View>
      </TouchableOpacity>
    );
  }
  handleSearch(value) {
    this.setState({
      search: value,
    });
    if (timeOut) {
      clearTimeout(timeOut);
    }
    timeOut = setTimeout(() => {
      this.setState({
        filterList: this.state.data.filter(
          (item) =>
            item.id.toLowerCase().includes(value) ||
            item.description.toLowerCase().includes(value) ||
            item.name.toLowerCase().includes(value),
        ),
      });
    }, 500);
  }
  onDelete(){
    this.setState({
      data:this.state.data.filter(item=>item!=this.state.id),
      filterList:this.state.filterList.filter(item=>item!=this.state.id),
      id:"",
      modal:false
    })
  }
  logout(){
    AsyncStorage.clear()
    this.props.navigation.navigate("Login")
  }
  render() {
    var styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: 'center',
      },
      input: {
        flex: 1,
      },
      searchView: {
        width: wp('90%'),
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
      },
      emptyView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      },
      buttonView: {
        width: wp('45%'),
        height: hp('5%'),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#5e994d',
      },
      buttonText: {
        color: '#FFF',
      },
      deleteIcon: {
        color: '#5e994d',
        fontSize: wp('30%'),
      },
      deleteText: {
        fontSize: wp('4%'),
        textAlign: 'center',
        fontWeight: 'bold',
      },
      name: {
        color: '#5e994d',
        fontWeight: 'bold',
      },
      modalContent:{
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center',
        width: wp('90%'),
        paddingTop: wp('5%'),
      },
      modalContainer:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f3f3f3c2',
      }
    });
    if(this.state.loading){
      return (
        <Loading/>
      )
    }
    return (
      <Container style={styles.container}>
        <Header style={{backgroundColor: '#5e994d'}}>
          <Left>
            <Text style={styles.buttonText}>List View</Text>
          </Left>
          <Right>
          <TouchableOpacity  onPress={() => this.logout()}>
            <Icon
              style={{
                color: '#FFF',
              }}
              type="AntDesign"
              name={'logout'}
            />
            </TouchableOpacity>
          </Right>
        </Header>
        <View style={styles.searchView}>
          <TextInput
            style={styles.input}
            value={this.state.search}
            underlineColorAndroid="transparent"
            placeholder="Search"
            autoCapitalize="none"
            onChangeText={(search) => {
              this.handleSearch(search);
            }}
          />
          <TouchableOpacity
            onPress={() => {
              if (this.state.search != '') {
                this.setState({
                  filterList: this.state.data,
                  search: '',
                });
              }
            }}>
            <Icon
              style={{
                color: '#C0C0C0',
              }}
              type="FontAwesome"
              name={this.state.search != '' ? 'close' : 'search'}
            />
          </TouchableOpacity>
        </View>
        {this.state.filterList.length > 0 ? (
          <FlatList
            style={{flex: 1}}
            data={this.state.filterList}
            renderItem={({item, index}) => this.renderItem(item, index)}
          />
        ) : (
          <View style={styles.emptyView}>
            <Text>no data found</Text>
          </View>
        )}
        <Modal
          visible={this.state.modal}
          transparent={true}
          modalDidOpen={() => console.log('modal did open')}
          modalDidClose={() => this.setState({modal: false})}>
          <View
            style={styles.modalContainer}>
            <View
              style={styles.modalContent}>
              <Text style={styles.deleteText}>
                Are you sure want to delete{' '}
                <Text style={styles.name}>{this.state.id.name}</Text>
              </Text>
              <Icon
                style={styles.deleteIcon}
                type="MaterialIcons"
                name="delete"
              />

              <View style={styles.searchView}>
                <TouchableOpacity
                  onPress={() => this.setState({id: '', modal: false})}
                  style={[styles.buttonView, {backgroundColor: '#5e994d2e'}]}>
                  <Text style={[styles.buttonText, {color: '#5e994d'}]}>
                    Cancel
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.onDelete()}
                  style={styles.buttonView}>
                  <Text style={styles.buttonText}>Delete</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </Container>
    );
  }
}
export default DashboardScreen;
