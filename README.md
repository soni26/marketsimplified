# Market Simplified-Android
Assuming that you have Android Studio(IDE) and Node 10 LTS or greater installed, you can use npm to install the Expo CLI command line utility:
	`npm install -g react-native-cli`
	
	
open Terminal and navigate to project folder 

## Metro builder
- ##  Setup

	`npm install`
	`react-native-link`
	`npx jetify`

- ##  Starting the App
	`npm start`

Open new terminal and navigate to project folder
## Android 
- ##  Setup

	`Set java path
	Set android sdk path`

- ##  Starting the App
	`react-native run-android`

## Build For Production
- ## .abb
    `cd android && ./gradlew bundleRelease`
- ## .apk
   `cd android && ./gradlew assembleRelease`





## User Data Base

 Below sample Data Used for Authendication 

```
const user = [
        {
          userName: 'user1',
          password: 'user@1',
        },
        {
          userName: 'user2',
          password: 'user@2',
        },
];

```
## Flow
    Login with valid credentials and its move to dashboard screen
	Delete popup will show if item long Pressed 
	In popup there is a two option for cancel the action and delete the item
	Logout option in top right corner of header
	Search item by id , name and description
	App directly move to dashboard screen the user already exist in app
	local storage will delete once user click logout
## Packages

`native-base`  - It is an open source framework to build react native apps for ANDROID and IOS , used for some of the basic components ( Button , Icon , Card views , etc )

`react`  - The react package contains only the functionality necessary to define React components

`react-native`  - React NAtive combines the best parts of native development with React , a best-in-class JavaScript library for building interfaces

`react-native-responsive-screen` (1.2.2) - To handle both Landscape and Portrait views

`react-navigation` (3.11.0) - Used for sidebar navigation setup

